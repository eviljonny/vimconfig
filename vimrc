" Enable pathogen
execute pathogen#infect()

" Graphical options
set background=dark
set nu
set shiftwidth=2
set tabstop=2
set softtabstop=2
set expandtab                     "Convert all tabs typed to spaces
set shiftround                    "Indent/outdent to nearest tabstop
set nowrap
set autoindent
set smartindent
set ruler
set hlsearch
set textwidth=119                  "Wrap at this column
"set relativenumber
"set lazyredraw                     "Definitely have this on with relative number or it's slooooooow
filetype plugin indent on

" Open splits right and below automatically
set splitright
set splitbelow

let mapleader = "-"

setl sw=2 ts=2 sts=2 et

" Set shiftwidth and tabstop to 4 for python and assembley
au FileType python setl shiftwidth=4 tabstop=4 softtabstop=4 et
au FileType asmM6502 setl shiftwidth=4 tabstop=4 softtabstop=4 et
au FileType php setl noet
au FileType cpp setl sw=4 ts=4 sts=4 et

"Allow % to bounce between angles too
set matchpairs+=<:>

" Allow backspace to work better
set backspace=indent,eol,start

" Syntax highlighting
syntax enable
colorscheme solarized

" Settings for ALE (async lint engine)
let g:ale_sign_column_always = 1
let g:ale_linters = {'asm': [], 'javascript': ["standard"]}

" Allow clicking and scrolling with the mouse when in normal mode only
set mouse=n

" Folding
set foldlevelstart=0
set foldmethod=indent

" Mappings
"" Clear search
nnoremap <silent> _ :nohl<CR>

"" Folding
nnoremap <Space> za
vnoremap <Space> za
nnoremap <S-Up> za
nnoremap <S-Down> za
nnoremap <S-Left> zA
nnoremap <S-Right> zR

"" Moving around split panes
map <Esc>[1;9C <C-w><Right>
map <Esc>[1;9D <C-w><Left>
map <Esc>[1;9A <C-w><Up>
map <Esc>[1;9B <C-w><Down>

"" Open a file under the cursor in a vertical tab
map <c-w>F :vertical wincmd f<CR>

"" Syntax oddities warnings
""" Match text after 120 characters blue.
:match DiffAdd '\%>120v.*'

""" Define trailing whitespace highlighting
highlight BadWhitespace ctermbg=red guibg=red

""" Warn about any trailing whitespace
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h,*.pm,*.pl,*.rb,*.js,*.html,*.erb,*.haml,*.tf match BadWhitespace /\s\+$/

" When opening files jump to the line you had it open on last
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Turn off taglist by default
let Tlist_Auto_Open = 0
" Exit when the tag list is the only split left
let Tlist_Exit_OnlyWindow = 1

" Only show alpha marks
let g:showmarks_include="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

"Change showmarks colours
highlight ShowMarksHLl ctermfg=green ctermbg=black cterm=bold
highlight ShowMarksHLu ctermfg=red ctermbg=black cterm=bold
highlight ShowMarksHLo ctermfg=blue ctermbg=black cterm=bold
highlight ShowMarksHLm ctermfg=blue ctermbg=black cterm=bold

" Turn off some things when in vimdiff
if &diff
    "" Turn off taglist
    let Tlist_Auto_Open = 0 
endif

" Set vim-rspec command
let g:rspec_command = "!bundle exec rspec {spec}"

" Set vim-rspec keys
map <Leader>t :call RunCurrentSpecFile()<CR>
map <Leader>s :call RunNearestSpec()<CR>
map <Leader>l :call RunLastSpec()<CR>
map <Leader>a :call RunAllSpecs()<CR>

" Set spellcheck options
highlight SpellBad cterm=underline
set spell spelllang=en_gb
set spellfile=~/.vim/spell/custom-wordlist.add

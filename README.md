Description
===========
This repo is just a place to store my vim config.

Pre-reqs
========

taglist
-------
To use taglist you need to install exuberant-ctags

- `[sudo] apt-get install exuberant-ctags`

vim-instant-markdown
--------------------
For the vim instant markdown plugin you need to follow the installation
instructions on the projects github page:
https://github.com/suan/vim-instant-markdown#installation

The pertinant ones are reproduced below (the other steps are not needed):

You first need to have Ruby with RubyGems, and node.js with npm installed. (In
the future there might be a version which won't require node.js at all, making
installation easier)

- `[sudo] apt-get install ruby ruby-dev npm`
- `[sudo] gem install pygments.rb`
- If you're using Ruby 1.9.2 or later (which is default on ubuntu 14.04), `[sudo] gem install redcarpet`.
  Otherwise, `[sudo] gem install redcarpet -v 2.3.0`
- `[sudo] npm -g install instant-markdown-d`
- If you're on Linux, the `xdg-utils` package needs to be installed (is
    installed by default on Ubuntu).

Syntastic syntax checker
------------------------

### Ruby

Install rubocop

### Python install pyflakes

On OSX

    sudo pip install pyflakes
